<!DOCTYPE html>
<script>
function addShadows() {
    var images = document.getElementsByClassName("border");

	for (let image of images) {
		image.classList.add("shadow_class");
	}
}

function removeShadows() {
    var images = document.getElementsByClassName("border");

	for (let image of images) {
		image.classList.remove("shadow_class");
	}
}
</script>

<html lang="en">

<head>
    <title>
        Assignment 05
    </title>
    <meta charset="UTF-8">
    <link rel="icon" type="image/png"  href="images/favicon-32x32.png">
    <link rel="icon" type="image/png"  href="images/favicon-16x16.png">
    <link rel="stylesheet" type="text/css" href="CSS/style1.css">
    <style>
        body {
            background-image: url("images/tile.png");
        }
    </style>
</head>

<body>
    <div id="container">
        <header>
            <h1>Three Column Layout</h1>
        </header>
        <nav class="column_element_nav">
            <ul>
                <li><a href="#">One</a></li>
                <li><a href="#">Two</a></li>
                <li><a href="#">Three</a></li>
                <li><a href="#">Four</a></li>
            </ul>
        </nav>
        <main class="column_element_main">
            <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy text ever
                since the 1500s, when an unknown printer took a galley of type and
                scrambled it to make a type specimen book. It has survived not only
                five centuries, but also the leap into electronic typesetting,
                remaining essentially unchanged. It was popularised in the 1960s
                with the release of Letraset sheets containing Lorem Ipsum passages,
                and more recently with desktop publishing software like Aldus
                PageMaker including versions of Lorem Ipsum.
            </p>
            <br>
            <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy text ever
                since the 1500s, when an unknown printer took a galley of type and
                scrambled it to make a type specimen book. It has survived not only
                five centuries, but also the leap into electronic typesetting,
                remaining essentially unchanged. It was popularised in the 1960s
                with the release of Letraset sheets containing Lorem Ipsum passages,
                and more recently with desktop publishing software like Aldus
                PageMaker including versions of Lorem Ipsum.
            </p>
            <br>
            <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy text ever
                since the 1500s, when an unknown printer took a galley of type and
                scrambled it to make a type specimen book. It has survived not only
                five centuries, but also the leap into electronic typesetting,
                remaining essentially unchanged. It was popularised in the 1960s
                with the release of Letraset sheets containing Lorem Ipsum passages,
                and more recently with desktop publishing software like Aldus
                PageMaker including versions of Lorem Ipsum.
            </p>
        </main>
        <aside class="column_element_aside">
            <h2>
                Four Buttons
            </h2>
            <img src="images/10.jpg" alt="Lady Staring back at you" class="border"><br>
            <img src="images/20.jpg" alt="Lady in a green dress sitting dowm and holding a book" class="border"><br>
            <img src="images/30.jpg" alt="Person with an angry face staring at you" class="border"> <br>
            <img src="images/40.jpg" alt="Woman holding her child with a blue background" class="border"> <br>
            <button type="button" onClick=addShadows()>Shadow On</button>
            <button type="button" onclick=removeShadows()>Shadow Off</button>
        </aside>
        <footer>
            <p>Copyright</p>
        </footer>
    </div>
</body>
</footer>